#### Loading models ####
import joblib

####  NLP modules #####
import nltk

### Load nltk ressources ###
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

from nltk.stem.snowball import EnglishStemmer
from nltk.stem import WordNetLemmatizer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
import re

import numpy as np
from gensim.models import Word2Vec

from models import Question, Prediction


#### Loading models, vectorizer and multilabel_binarizer ####
MODEL = joblib.load('pkls/sgd_classifier.pkl')
VECTORIZER = joblib.load('pkls/tfidf_vectorizer.pkl')
ML = joblib.load('pkls/multilabel_binarizer.pkl')
W2VEC = Word2Vec.load("pkls/word2vec.model")


def get_wordnet_pos(treebank_tag):
    """
    adjust the lemmatizer with the grammatical type
    J: Adjectif
    V: Verb
    N: Common name
    R: Adverb
    """

    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return 'n'

TO_KEEP = ['c#','c++','c']

def prepare_text(text: str) -> str:
    """
    Tokenize and prepare the vectorization
    """
   ### English stopwords ###
    SWE = tuple(nltk.corpus.stopwords.words('english'))
    
    to_keep = [t_k for t_k in TO_KEEP if t_k in str(text)]
            
     ### tokenizer ###
    tokenizer = nltk.RegexpTokenizer(r'\w*\.?\w+[#+]*')
    p_token = tokenizer.tokenize(str(text))
    
    
    ### lemmatizer ###
    lemmatizer = WordNetLemmatizer()
    p_token = [lemmatizer.lemmatize(l[0],get_wordnet_pos(l[1])) for l in nltk.pos_tag(p_token) if l not in SWE]
    
    ### Stemmer ###
    stemmer = EnglishStemmer()
    p_token = [stemmer.stem(token) for token in p_token if (token not in SWE)
               and ((len(stemmer.stem(token))>1) or stemmer.stem(token) in TO_KEEP)]
    
    
    return p_token

def get_best_tags(X, n_tags=5):
    decfun = MODEL.decision_function(X)
    best_tags = np.argsort(decfun)[:, :-(n_tags+1): -1]
    return ML.classes_[best_tags]

def get_key_words(question: str, n_tags: int) -> list:
    """
    predict a preprocessed question ('body' + 'title')
    with the word2vec algorithm and give the number of key_words wanted
    
    question: preprocessed question with to_str() function
        
    """
    return [i[0] for i in W2VEC.predict_output_word(question, topn=n_tags)]


def prediction(question: Question) -> Prediction:
    """
    prepare a Question and make supervised and unsupervised prediction
    returns Predition
    """
    title = question.title
    body = question.body
    
    q0 = title + body
    q1 = prepare_text(q0)

    q2 = VECTORIZER.transform([' '.join(q1)])

    pred1 = list(get_best_tags(q2, question.n_tags)[0])
    pred2 = get_key_words(q1,question.n_tags)

    res = Prediction(supervised=pred1, unsupervised=pred2)
    
    return res




