from fastapi import FastAPI

from models import Question, Prediction

from prediction import prediction    

description ="""
Stackoverflow-tags will help you to find some TAGS when you ask a question on [https://stackoverflow.com/](https://stackoverflow.com/)

This API uses a supervised and an unsupervised machin learning model.

You will be able to compare results.
"""
tags_metadata = [
    {
        "name": "Prediction",
        "description": "Post your question here, and get TAGS",
    }
]

app = FastAPI(
    openapi_tags=tags_metadata,
    title="StackOverflow-Tags",
    description=description
)




@app.post("/predict_tags/", response_model=Prediction, tags=['Prediction'] )
async def predict(question: Question):
    print('salut')
    return prediction(question)