from pydantic import BaseModel
from typing import Optional

class Question(BaseModel):
    title: str = ""
    body: str = ""
    n_tags: Optional[int] = 5

    class Config:
        schema_extra = {
            "example": {
                "title": "Install python module",
                "body": "How to install a python package?",
                "n_tags": 5,
            }
        }

class Prediction(BaseModel):
    supervised: list
    unsupervised: list

    class Config:
        schema_extra = {
            "example": {
                "supervised": [
                    "python",
                    "python-2.7",
                    "python-3.x",
                    "tensorflow",
                    "matplotlib"
                ],
                "unsupervised": [
                    "pip",
                    "instal",
                    "packag",
                    "python",
                    "virtualenv"
                ]

            }
        }