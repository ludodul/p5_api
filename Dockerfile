FROM python:3.9
   
COPY pip-requirements.txt pip-requirements.txt

# Install Python setuptools
RUN pip3 install -U -r pip-requirements.txt
    
RUN mkdir /app
WORKDIR /app

COPY pkls /app/pkls
COPY main.py /app/main.py
COPY models.py /app/models.py
COPY prediction.py /app/prediction.py

CMD uvicorn main:app --host 0.0.0.0