## Catégoriser automatiquement des questions

API de suggestion de tags pour [Stackoverflow](https://stackoverflow.com)  

Déploiement local:

Création de l'environnement virtuel
```
python -m venv env
```
Activation de l'environnement
```
source env/bin/activate
```
Installation des dépendances
```
pip install -r pip-requirements.txt
```
Lancement de l'api
```
uvicorn main::app --reload
```
[http://127.0.0.1:8000](http://127.0.0.1:8000)

Déploiement en ligne:

Modification du Caddyfile
```txt
mon.domaine {
    tls mon.email@mail.com
    proxy / http://api:8000 {
    transparent
    }
}
```
Lancement de docker-compose
```
docker-compose build
docker-compose up
```

